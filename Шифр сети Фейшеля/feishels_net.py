import sys
from itertools import cycle

ROUND_COUNT = 8
SECRET_KEY = "IB"
FILL_SYMBOL = " "

ALPHABET = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789. "
char2code = {char: i for i, char in enumerate(ALPHABET)}
code2char = {i: char for i, char in enumerate(ALPHABET)}

def xor(text1: str, text2: str) -> str:
    xored = [code2char[char2code[text1[i]] ^ char2code[text2[i]]] for i in range(len(text1))]
    return "".join(xored)


def add_function(text: str, key: str) -> str:
    added = [code2char[(char2code[text[i]] + char2code[next(key)]) % len(ALPHABET)] for i in range(len(text))]
    return "".join(added)


class FeishelsNetCipher:
    def __init__(self, rounds: int, secret_key: str, fill_symbol: str):
        self.rounds = rounds
        self.key = secret_key
        self.fill_symbol = fill_symbol

    def _crypt(self, text_left: str, text_right: str, K1: str, K2: str) -> tuple:
        left, right = text_left, text_right

        for i in range(self.rounds):
            K1ROLi = K1[i % 6:] + K1[:i % 6]
            K2RORi = K2[-i % 6:] + K2[:-i % 6]
            round_key = code2char[char2code[K1ROLi] ^ char2code[K2RORi]]

            f_result = add_function(right, cycle(round_key))

            new_left = xor(left, f_result)

            left, right = right, new_left

        return left, right

    def encode(self, text: str) -> str:
        K1, K2 = self.key[0], self.key[1]
        if len(text) % 2 > 0:
            text += self.fill_symbol
        text_left, text_right = text[:len(text) // 2], text[len(text) // 2:]
        r, l = self._crypt(text_left, text_right, K1, K2)
        return l + r

    def decode(self, text: str) -> str:
        K1, K2 = self.key[0], self.key[1]
        if len(text) % 2 > 0:
            text += self.fill_symbol
        text_left, text_right = text[:len(text) // 2], text[len(text) // 2:]
        l, r = self._crypt(text_left, text_right, K1, K2)
        return r + l



if __name__ == '__main__':
    cipher = FeishelsNetCipher(ROUND_COUNT, SECRET_KEY, FILL_SYMBOL)  # создаем экзепмляр класса-шифровальщика

    message = ''
    for i in sys.argv[1:]:
        if not message:
            message = i
        else:
            message += f" {i}"

    if not message:
        message = input("Введите сообщение для кодирования: ")

    encoded = cipher.encode(message)
    print("Зашифрованная строка: ", encoded)
    print("Расшифрованная строка: ", cipher.decode(encoded))