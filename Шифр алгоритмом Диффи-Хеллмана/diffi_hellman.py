import sys


def is_prime(number: int) -> bool:
    k = 0
    for i in range(2, number // 2+1):
        if number % i == 0:
            k += 1
    return k <= 0


class DiffiHellManConnector:
    def __init__(self, a, b, p, point, k1, k2):
        self.a = a
        self.b = b

        if not is_prime(p):
            print('p должно быть простым, попробуйте ещё раз')
            exit()
        self.p = p

        self.check_point(point)
        self.point = point

        self.K1 = self.double_and_add(k1)
        self.K2 = self.double_and_add(k2)
        self.pubkey1 = self.double_and_add(k2)
        self.pubkey2 = self.double_and_add(k1)

    def __str__(self):
        to_print = f"a = {self.a}\n" + \
                   f"b = {self.b}\n" + \
                   f"p = {self.p}\n" + \
                   f"point = {self.point}\n" + \
                   f"k1 = {self.K1}\n" + \
                   f"k2 = {self.K2}\n" + \
                   f"Public key1: {cipher.pubkey1}, public key2: {cipher.pubkey2}"
        return to_print

    def check_point(self, point: tuple):
        if point[1] ** 2 != point[0] ** 3 + self.a * point[0] + self.b % self.p:
            print(f"Точка {point} не принадлежит данной кривой, попробуйте ещё раз")
            exit()

    def multiplicative_inverse(self, m) -> float | int:
        return pow(self.a, -1, m)

    def double(self, x1, y1):
        s = ((3 * (x1 ** 2) + self.a) * self.multiplicative_inverse(2 * y1)) % self.p
        x3 = (s ** 2 - x1 - x1) % self.p
        y3 = (s * (x1 - x3) - y1) % self.p
        return x3, y3

    def add(self, x1, y1, x2, y2):
        s = 0
        if x1 == x2:
            s = ((3 * (x1 ** 2) + self.a) * self.multiplicative_inverse(2 * y1)) % self.p
        else:
            s = ((y2 - y1) * self.multiplicative_inverse(x2 - x1)) % self.p
        x3 = (s ** 2 - x1 - x2) % self.p
        y3 = (s * (x1 - x3) - y1) % self.p
        return x3, y3

    def double_and_add(self, scalar):
        result = (0, 0)
        while scalar != 0:
            if scalar % 2 == 1:
                result = self.add(*result, *self.point)
            point = self.double(*self.point)
            scalar //= 2
        return result


if __name__ == '__main__':
    cipher = DiffiHellManConnector(a=-1, b=1, p=89, point=(5, 11), k1=5, k2=19)

    print(cipher)

