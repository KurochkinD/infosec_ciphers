import sys

a = 5
b = 6
c = 3
t0 = 7


class LinearCongruentialCipher:
    def __init__(self, a, b, c, t0):
        self.a = a
        self.b = b
        self.c = c
        self.m = int(2**b)
        self.t0 = t0

    def encode(self, s) -> str:
        assert len(s) % self.b == 0
        k = len(s) // self.b
        t = [self.t0]
        for i in range(k):
            t.append((self.a*t[i] + self.c) % self.m)
        answer = []
        for i in range(k):
            chunk = s[i*self.b:(i+1)*self.b]
            dec = int(chunk, 2)
            enc_dec = dec ^ t[i+1]
            enc_bin = f"{enc_dec:06b}"
            answer.append(enc_bin)
        return "".join(answer)

    def decode(self, s) -> str:
        return self.encode(s)


if __name__ == '__main__':
    cipher = LinearCongruentialCipher(a, b, c, t0)
    message = ''
    for i in sys.argv[1:]:
        if not message:
            message = i
        else:
            message += f" {i}"

    if not message:
        message = input("Введите сообщение для кодирования: ")

    encoded = cipher.encode(message)
    print("Зашифрованная строка: ", encoded)
    print("Расшифрованная строка: ", cipher.decode(encoded))



