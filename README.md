# Лабораторные работы по шифрованию



## Курочкин Даниил, 11-006


#### Каждая лабораторная работа представлена в отдельной папке, в которой содержится:
- написанный код
- инструкция, объясняющая принцип работы данного шифра и алгоритм запуска программы
- скриншот с демонстрацией работы алгоритма, отображаемый в инструкции
