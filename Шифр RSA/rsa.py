import sys
from math import gcd
from random import randrange


def generate_key_pair(p, q):
    n = p * q
    phi = (p - 1) * (q - 1)

    e = randrange(1, phi)
    while gcd(e, phi) != 1:
        e = randrange(1, phi)

    d = pow(e, -1, phi)

    return (e, n), (d, p, q)


class RSACipher:
    def __init__(self, p, q):
        self.public_key, self.private_key = generate_key_pair(p, q)

    def encode(self, s):
        e, n = self.public_key
        ciphertext = [chr(pow(ord(char), e, n)) for char in s]
        return ''.join(ciphertext)

    def decode(self, s):
        d, p, q = self.private_key
        n = p * q
        plaintext = [chr(pow(ord(char), d, n)) for char in s]
        return ''.join(plaintext)


if __name__ == '__main__':
    p = 89
    q = 83
    cipher = RSACipher(p, q)  # создаем экзепмляр класса-шифровальщика

    message = ''
    for i in sys.argv[1:]:
        if not message:
            message = i
        else:
            message += f" {i}"

    if not message:
        message = input("Введите сообщение для кодирования: ")

    encoded = cipher.encode(message)
    print("Зашифрованная строка: ", encoded)
    print("Расшифрованная строка: ", cipher.decode(encoded))
