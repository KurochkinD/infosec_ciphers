import sys


class DoublePermutationCipher:
    def __init__(self):
        self.key = [int(x) for x in "3 1 4 5 2".split()]
        self.key_len = len(self.key)
        self.index_key = [self.key.index(s) + 1 for s in range(1, self.key_len + 1)]

    def encode(self, message):

        if len(message) % self.key_len != 0:
            message += '0' * (self.key_len - len(message) % self.key_len)

        enc_msg = ''
        for i in range(0, len(message), self.key_len):
            for j in range(self.key_len):
                enc_msg += message[i + self.key[j % self.key_len] - 1]

        encoded = ''
        for i in range(self.key_len):
            for j in range(len(enc_msg) // self.key_len):
                encoded += enc_msg[i + self.key_len * j]

        return encoded

    def decode(self, encoded_text):
        CT_LEN = len(encoded_text)

        decoded_seq = ''
        for i in range(CT_LEN // self.key_len):
            for j in range(self.key_len):
                decoded_seq += encoded_text[i + (CT_LEN // self.key_len) * j]

        decoded = ''
        for i in range(0, CT_LEN, self.key_len):
            for j in range(self.key_len):
                decoded = decoded + decoded_seq[i + self.index_key[j % self.key_len] - 1]

        decoded = decoded.replace("0", "")
        return decoded


if __name__ == '__main__':
    cipher = DoublePermutationCipher()  # создаем экзепмляр класса-шифровальщика

    message = ''
    for i in sys.argv[1:]:
        if not message:
            message = i
        else:
            message += f" {i}"

    if not message:
        message = input("Введите сообщение для кодирования: ")

    encoded = cipher.encode(message)
    print("Зашифрованная строка: ", encoded)
    print("Расшифрованная строка: ", cipher.decode(encoded))
